import './shoes.scss';
import ShoesSlider from './shoes.script';

window.addEventListener('DOMContentLoaded', () => {
  if (document.querySelector('.shoes__slider .swiper-container')) {
    new ShoesSlider('.shoes__slider .swiper-container').init();
  }
});
