import { Autoplay, Navigation, Swiper, SwiperOptions } from 'swiper';

export class ShoesSlider {
  swiper: Swiper;

  private readonly container: string;

  constructor(container: string) {
    this.container = container;
  }

  public init(): void {
    Swiper.use([Navigation, Autoplay]);

    const swiperParams: SwiperOptions = {
      spaceBetween: 0,
      loop: true,
      slidesPerView: 1,
      centeredSlides: true,
      speed: 300,
      autoplay: {
        delay: 3000,
      },
      navigation: {
        prevEl: '.shoes__slider-nav-btn--prev',
        nextEl: '.shoes__slider-nav-btn--next',
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          centeredSlides: false,
        },

        992: {
          slidesPerView: 3,
        },
      },
    };

    this.swiper = new Swiper(this.container, swiperParams);
  }
}

export default ShoesSlider;
