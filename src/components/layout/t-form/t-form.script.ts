class TForm {
  private form: HTMLFormElement;

  constructor(form: string) {
    this.form = document.querySelector(form);
  }

  init(): void {
    this.checkAll();
  }

  checkAll(): void {
    const checkAllCheckbox: HTMLInputElement = this.form.querySelector('#check-all');
    const formCheckboxes: HTMLInputElement[] = Array.from(
      document.querySelectorAll('input.form-check-input:not(#check-all)')
    );

    checkAllCheckbox.addEventListener('change', () => {
      if (checkAllCheckbox.checked === true) {
        formCheckboxes.forEach((checkbox: HTMLInputElement) => {
          // eslint-disable-next-line no-param-reassign
          checkbox.checked = true;
        });
      } else {
        formCheckboxes.forEach((checkbox: HTMLInputElement) => {
          // eslint-disable-next-line no-param-reassign
          checkbox.checked = false;
        });
      }
    });
  }
}

export default TForm;
