import './t-form.scss';
import TForm from './t-form.script';

window.addEventListener('DOMContentLoaded', () => {
  if (document.querySelector('#t-form')) {
    new TForm('#t-form').init();
  }
});
