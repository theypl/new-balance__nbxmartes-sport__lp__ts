import './t-shirts.scss';
import TshirtsSliders from './t-shirts.script';

window.addEventListener('DOMContentLoaded', () => {
  if (document.querySelector('.t-shirts__slider .swiper-container')) {
    new TshirtsSliders('.t-shirts__slider .swiper-container').init();
  }
});
